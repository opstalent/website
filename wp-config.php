<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'apps');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'C//5y&Gl!!t!D-q+i>RRgnA7e3!sOjL.R`GTA-:6Bn0EkW)JwSnUz==H68)q;-lJ');
define('SECURE_AUTH_KEY',  'i;rB4(hAM.-BB7R/M/pjK$8~?$$@`ponax8gSHV>%i5VBg8gwA7S,NJL@f#,4hc%');
define('LOGGED_IN_KEY',    '^xIprX=lXzb7ZrS;`gR,%M.v8e!az+G/(Wcwjz<NmYzoVKlHJ IS#o<hRos$@+ZB');
define('NONCE_KEY',        '1>D.YR9-1oz*$1_*|j5I#8_w{-eF=<,5D|wcKDClWqN36UnyM/1#cQvs6hWFfD/m');
define('AUTH_SALT',        '!<7:u4-<4`h^3DXZyU,FV%b~4Kzbn*w7e^Ob4M.&6WcU5G/DjH*Op2bno-u|[Cq1');
define('SECURE_AUTH_SALT', '(vjKGciq`:jY](DY{X(c~STOIQsJq%8y!VI,A.-h;`/hT;A6reJ i?;i%ar_jnzf');
define('LOGGED_IN_SALT',   'Z(k;oN32^4[XlG`jv`_cYvl1al6UG|VmF0A7vwuZc+_{qhruV)kW3T-2Yx<StV%.');
define('NONCE_SALT',       'lj%,A5Xas_{f<P=-X92{yYpG=lE$-LOL?MN5vL;^f>I, }&.PE)-T2[ Fhg6f.o.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
